SUMMARY = "Example recipe for first application"
DESCRIPTION = "This recipe serve as an example of hello world"
SECTION = "simha_world"
PN = "simha"
PV = "0.0.1"
PR = "r1"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

SRC_URI = "file://simha.c"

S = "${WORKDIR}"

do_compile(){
	${CC} ${LDFLAGS} simha.c -o simha
}

do_install(){
	install -d ${D}${bindir}
	install -m 0755 simha ${D}${bindir}
}

