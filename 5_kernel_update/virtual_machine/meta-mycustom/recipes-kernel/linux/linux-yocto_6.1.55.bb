DESCRIPTION = "Linux kernel"
SECTION = "kernel"
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM ?= "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

KBRANCH ?= "v6.1/standard/base"

LINUX_VERSION = "6.1.55"
LINUX_VERSION_EXTENSION = "-techveda"
DEPENDS += "bc-native bison-native util-linux-native xz-native lz4-native"

inherit kernel

PV = "${LINUX_VERSION}"

SRC_URI = "git://git.yoctoproject.org/linux-yocto;branch=${KBRANCH}"
SRCREV = "644e73fa089ade30c0db7bd54be960be92546ba2"
S = "${WORKDIR}/git"

KERNEL_DEVICETREE_qemuarm = "versatile-pb.dtb"
KERNEL_DEVICETREE_beaglebone_yocto = "am355x-boneblack.dtb"

KERNEL_CONFIG_COMMAND_qemuarm = "oe_runmake -C ${S} CC="${KERNEL_CC}" LD="${KERNEL_LD}" O=${B} multi_v7_defconfig"
KERNEL_CONFIG_COMMAND_beaglebone-yocto = "oe_runmake -C ${S} CC="${KERNEL_CC}" LD="${KERNEL_LD}" O=${B} omap2plus_defconfig"


COMPATIBLE_MACHINE = "(qemuarm|beaglebone-yocto)"
