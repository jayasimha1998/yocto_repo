DESCRIPTION = "Linux kernel"
SECTION = "kernel"
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM ?= "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"


LINUX_VERSION = "6.4.3"
LINUX_VERSION_EXTENSION = "-techveda"
DEPENDS += "bc-native bison-native util-linux-native xz-native lz4-native"

inherit kernel

PV = "${LINUX_VERSION}"

SRC_URI = "https://mirrors.edge.kernel.org/pub/linux/kernel/v6.x/linux-6.4.3.tar.gz"

SRC_URI[sha256sum] = "56eb840f0a0ae6956ca2f3e67e0c27285b8a1d4a1121c2816843bfede772de45"

#S = "${WORKDIR}/git"
S = "${WORKDIR}/linux-${PV}"

KERNEL_DEVICETREE_qemuarm = "versatile-pb.dtb"
KERNEL_DEVICETREE_beaglebone_yocto = "am355x-boneblack.dtb"

KERNEL_CONFIG_COMMAND_qemuarm = "oe_runmake -C ${S} CC="${KERNEL_CC}" LD="${KERNEL_LD}" O=${B} multi_v7_defconfig"
KERNEL_CONFIG_COMMAND_beaglebone-yocto = "oe_runmake -C ${S} CC="${KERNEL_CC}" LD="${KERNEL_LD}" O=${B} omap2plus_defconfig"


COMPATIBLE_MACHINE = "(qemuarm|beaglebone-yocto)"
